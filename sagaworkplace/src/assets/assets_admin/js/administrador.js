$(document).ready(function () {
    var windowHeight = $(window).height() - 180;
    var href = "";
    var currentNav = "";
    url = document.location.toString();
    heightWindow = $(window).height();


    $(".nav > li > a").click(function (e) {
        var href = $(this).attr("href");

        if (href != "#") {
            return scrollAnimate(href);
        }
    });


    function scrollAnimate(href) {
        var element = $(href);
        if (element.length > 0) {
            var position = element.offset().top - 70;


            $("html, body").animate({
                scrollTop: position
            }, 700);

            if ($(window).width() < 765) {
                $('.navbar-collapse').animate({
                    height: '1px'
                }, 'fast', function () {
                    $(this).removeClass('in');
                });
            }

            return false;
        } else {
            location.href = "/" + href;
        }
    }

    $( function() {
        $(".datepicker").datepicker({
            //defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
            yearRange: "-50: +nn",
            numberOfMonths: 2,
            dateFormat: 'dd-mm-yy'
        }).datepicker("setDate", new Date());

        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
    } );

});