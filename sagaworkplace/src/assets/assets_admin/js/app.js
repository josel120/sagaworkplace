$(document).ready(function () {
    var dataTable = $('.data-table').DataTable({
        language: {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No se encontró nada - lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)"
        }
    });

    $('.data-table tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input class="form-control" type="text" placeholder="Buscar ' + title + '" />');
    });

    // Apply the search
    dataTable.columns().every(function () {
        var that = this;

        $(this.footer()).find('input').on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });

    $('.data-table tfoot tr').appendTo('.data-table thead');

    /////
    $("#logout").on('click', function (e) {
        e.preventDefault();

        $("#logout-form").submit();
    });

    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    $(".isNumberDecimal").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault();
        //if a decimal has been added, disable the "."-button

    });
});