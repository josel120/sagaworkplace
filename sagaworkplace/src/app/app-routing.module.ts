import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
//import { TestComponent } from './test/test.component';
import { RegistroComponent } from './components/registro/registro.component';
import { MyTableComponent } from './components/my-table/my-table.component';
import { BienvenidosComponent } from './components/bienvenidos/bienvenidos.component';
import { TablaWorkplaceComponent } from './components/tabla-workplace/tabla-workplace.component';
import { MensajesComponent } from './components/mensajes/mensajes.component';
import { ColaboradoresComponent } from './components/colaboradores/colaboradores.component';
//import { AuthGuardService } from './services/guardia/auth-guard.service';

const APP_ROUTES: Routes = [
    {path:'', component: LoginComponent},
    {path:'login', component: LoginComponent},
    {path:'registro', component: RegistroComponent},
    {path:'colaboradores', component: ColaboradoresComponent},
    {path:'plataforma', component: RegistroComponent},
    {path:'workplace', component: TablaWorkplaceComponent},
    {path:'bienvenidos', component: BienvenidosComponent},
    {path:'mensajes', component: MensajesComponent},
    {path:'**', component: LoginComponent}
];

//export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);