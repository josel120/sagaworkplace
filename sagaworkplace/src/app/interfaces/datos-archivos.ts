export interface DatosArchivos {
    nombre: string;
    username: string;
    id: string;
    nivel:string;
    path:string;
}