import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
//import { APP_ROUTING } from './/app-routing.module';
import { routing, appRoutingProviders } from './/app-routing.module';
import { TestModule } from './test/test.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyTableComponent } from './components/my-table/my-table.component';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatInputModule, MatCheckboxModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegistroComponent } from './components/registro/registro.component';
import { MyNavComponent } from './components/my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { AdministradorComponent } from './components/administrador/administrador.component';
import { SoporteComponent } from './modal/soporte/soporte.component';
import { BienvenidosComponent } from './components/bienvenidos/bienvenidos.component';
import { TablaWorkplaceComponent } from './components/tabla-workplace/tabla-workplace.component';
import { MensajesComponent } from './components/mensajes/mensajes.component';
import { ColaboradoresComponent } from './components/colaboradores/colaboradores.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    MyTableComponent,
    LoginComponent,
    RegistroComponent,
    MyNavComponent,
    AdministradorComponent,
    SoporteComponent,
    BienvenidosComponent,
    TablaWorkplaceComponent,
    MensajesComponent,
    ColaboradoresComponent,
    SideNavComponent
  ],
  imports: [
    BrowserModule,
    TestModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    FormsModule,
    //APP_ROUTING,
    routing,
    NgxSpinnerModule,
    HttpClientModule,
    ToastrModule,
    ToastrModule.forRoot(),
    NgbModule.forRoot(),
    ReactiveFormsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDialogModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent],
  entryComponents:[SoporteComponent]
})
export class AppModule { }
