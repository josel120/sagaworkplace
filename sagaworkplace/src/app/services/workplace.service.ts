import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GLOBAL } from '../config/config-url';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WorkplaceService {private httpOptions;

    constructor(private http: HttpClient) {
        this.httpOptions = {
            headers: new HttpHeaders({
            //    'Content-Type':'text/plain',
            //    'Accept': 'application/json',
            //  'Content-Type':  'text/javascript',
            //    'Access-Control-Allow-Methods': 'GET',        
            //    'Access-Control-Allow-Origin': '*',
                //'Authorization': `Bearer DQVJzV3lmNWRnYzZAiYUN2NXpIWUFISERMeS1JQUdReFpvZAmc3S1MtZAnJEUWktdXNKM2h0dGN2R3NPRlc1Q2lWZAFByLWFBcmpKT3dOTDMwdzZAjbndaenE4RnF0RWUyNF9hek5NTVhkV3EzZAlp3U1RNM3hQNS1mOEZAKMjVoLWNMa2ItNjU3WTREeERpT1VfNDV1V3pSTkxWWmR6RW5BYkY3c0NTWm5kTmNvaXZAKQm03TnpiNUdsLUtHVXFDMlcwWnFVaThoclBB`
            //    'Access-Control-Allow-Credentials':'true',
            //    'Access-Control-Allow-Headers': '*',
            //    'Access-Control-Max-Age':'10'
                 
            })
        };
    }

    // Listar Usuarios WorkPlaces Facebook 
    /*
    listarUsers(count=10,startIndex=1): Observable<any>{
        this.httpOptions = {
            headers: new HttpHeaders({
                'Authorization': `Bearer DQVJzV3lmNWRnYzZAiYUN2NXpIWUFISERMeS1JQUdReFpvZAmc3S1MtZAnJEUWktdXNKM2h0dGN2R3NPRlc1Q2lWZAFByLWFBcmpKT3dOTDMwdzZAjbndaenE4RnF0RWUyNF9hek5NTVhkV3EzZAlp3U1RNM3hQNS1mOEZAKMjVoLWNMa2ItNjU3WTREeERpT1VfNDV1V3pSTkxWWmR6RW5BYkY3c0NTWm5kTmNvaXZAKQm03TnpiNUdsLUtHVXFDMlcwWnFVaThoclBB`
            })   
        };
        this.httpOptions = {headers: new HttpHeaders({
                "Access-Control-Allow-Methods": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Max-Age":"600",
                "Access-Control-Allow-Headers":"*",
                "http.Content-Length":"0",
                "Content-Type":"application/json",
                "Authorization": "Bearer DQVJzV3lmNWRnYzZAiYUN2NXpIWUFISERMeS1JQUdReFpvZAmc3S1MtZAnJEUWktdXNKM2h0dGN2R3NPRlc1Q2lWZAFByLWFBcmpKT3dOTDMwdzZAjbndaenE4RnF0RWUyNF9hek5NTVhkV3EzZAlp3U1RNM3hQNS1mOEZAKMjVoLWNMa2ItNjU3WTREeERpT1VfNDV1V3pSTkxWWmR6RW5BYkY3c0NTWm5kTmNvaXZAKQm03TnpiNUdsLUtHVXFDMlcwWnFVaThoclBB"
            })
        }
        //return this.http.get<any>(`${GLOBAL.apiUrlWorkplace}/?count=${count}&startIndex=${startIndex}`, this.httpOptions)
        return this.http.get<any>(`${GLOBAL.apiUrlWorkplace}`,this.httpOptions)    
            .pipe(map(res => res));
    }
    */
    listLUIS():Observable<any>{
        this.httpOptions = {headers: new HttpHeaders({
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Max-Age":"600",
            "Access-Control-Allow-Headers":"*",
            "http.Content-Length":"0",
            "Content-Type":"application/json"})}

        return this.http.get<any>(`${GLOBAL.apiUrlLUIS}/users`,this.httpOptions)    
            .pipe(map(res => res));
    }

}