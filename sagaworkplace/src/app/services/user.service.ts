import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GLOBAL } from '../config/config-url';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {private httpOptions;
    private loggedIn: Subject<boolean> = new Subject<boolean>();
    private mensajeSource = new BehaviorSubject<any>("Bienvenidos");
    currentMessage = this.mensajeSource.asObservable();

    constructor(private http: HttpClient) {
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':'application/json',
                'Access-Control-Allow-Methods': '*',        
                'Access-Control-Allow-Origin': '*',
                'Authorization': `Bearer ${localStorage.getItem('token')}` 
            })
          };
          this.loggedIn.next(!!localStorage.getItem('authToken'));
    }
    changeMessage(message: any){
        this.mensajeSource.next(message);
    }

    // Login en la plataforma
    login(data): Observable<any>{
        let params = JSON.stringify(data);
        //this.loggedIn.next(true);
        //console.log('login',this.loggedIn);
        return this.http.post<any>(`${GLOBAL.apiUrlLocal}/user/login`,params, this.httpOptions)
            .pipe(map(res => res));
    }
    /*
    cerrarSesion(){
        //this.loggedIn.next(false);
        console.log('cerrarSesion',this.loggedIn);
        return;
    }
    */

    // Registrar nuevo usuario
    registrarUser(data): Observable<any>{
        let params = JSON.stringify(data);
        return this.http.post<any>(`${GLOBAL.apiUrlLocal}/user/signup`,params, this.httpOptions)
            .pipe(map(res => res));
    }

    // Listar Usuarios Administradores
    listarAdmin(): Observable<any>{
        return this.http.get<any>(`${GLOBAL.apiUrlLocal}/user/list`, this.httpOptions)
            .pipe(map(res => res));
    }

    // Registrar nuevo usuario
    eliminarUser(data): Observable<any>{
        //let params = JSON.stringify(data._id);
        //console.log('params',data._id);
        return this.http.delete<any>(`${GLOBAL.apiUrlLocal}/user/${data._id}`, this.httpOptions)
            .pipe(map(res => res));
    }

    // make isLoggedIn public readonly
    get isLoggedIn() {
        return this.loggedIn.asObservable();
    }

}