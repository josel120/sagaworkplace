import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GLOBAL } from '../config/config-url';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, count } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class MongoDBService {
    private httpOptions;

    constructor(private http: HttpClient)  {
        this.httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
            //  'Content-Type':  'text/plain',
              'Access-Control-Allow-Methods': '*',        
              'Access-Control-Allow-Origin': '*',
              'Authorization': `bearer ${localStorage.getItem('token')}`
            })
          };
    }

    
    //Listar usuario de BD
    listUsersDB(): Observable<any>{
        return this.http.get<any>(`${GLOBAL.apiUrlLocal}/datacsv`, this.httpOptions)
            .pipe(map(res => res));
    }

    ingresarUsuarioDB(data): Observable<any>{
        let user = JSON.stringify(data);        
        var newObj = {};
        var cols = data.shift();
        let newArr = data.map(function(element,index){
            element.forEach(function(data,index){
                newObj[cols[index]]=data;
            });
            //console.log( newObj);
        });
        
        return this.http.post<any>(`${GLOBAL.apiUrlLocal}/datacsv`, newObj, this.httpOptions)
            .pipe(map(res => res));
            
    }

}