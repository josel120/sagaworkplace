export const GLOBAL = {
    apiUrlLocal: 'http://localhost:3000',
    apiUrlLUIS: 'http://localhost:6070',
    apiUrlWorkplace: 'https://www.facebook.com/scim/v1/Users',
    tokenJL2: 'DQVJzV3lmNWRnYzZAiYUN2NXpIWUFISERMeS1JQUdReFpvZAmc3S1MtZAnJEUWktdXNKM2h0dGN2R3NPRlc1Q2lWZAFByLWFBcmpKT3dOTDMwdzZAjbndaenE4RnF0RWUyNF9hek5NTVhkV3EzZAlp3U1RNM3hQNS1mOEZAKMjVoLWNMa2ItNjU3WTREeERpT1VfNDV1V3pSTkxWWmR6RW5BYkY3c0NTWm5kTmNvaXZAKQm03TnpiNUdsLUtHVXFDMlcwWnFVaThoclBB',
    tokenWorkplace: 'DQVJzT3dfOUpjMWlXdVE2TkRaWEVCZA3c3NmowMV84T3MxVFFxX0dMSXhsczVHc1hGdUQxbC16UkdLWGdWLWVJRnpPT2xsTWJ2WXg1NmZAySFFobXNmb3ZAxSnhaRDZARanBVUndnWkh2MmpIQXBYbHYtNWJoTEpHQUlFcW01b1VOcXhJT0hqbld6UE4tUVJQTXA0NWVzbG0xeDM5ZA0luQkUwckhUM1lESEo1UVdqcmdaQ2RJS2djTHNCbWpVMy10bkJnamFkVVRn',
    tokenJL: 'DQVJ2MW5meVBjdmUwVXFLNk5Hd1BWOFBxZAmFBU3BVekRqcXBkQmx4SzF4b25GYkZAqNEs5YzhkcGdEUl94Yy1Bd01zRzA3SUlSalA1QjhIdjQ3eV9rZAkpGSm42M0p5SHFKOGoyRjR5MDFwTmxiMndDVFA3RTVXRHBQalo5MkprYWp0ZAVZAYUVU5ZAHhfYWVlMWlvWlVCTGpzRm1hb1BQWDFNRkdJR1Nlb1BEVm5QWDlrTFlKWU45SFpiR0Jxem5OR3FSc2VZAeERR'
};