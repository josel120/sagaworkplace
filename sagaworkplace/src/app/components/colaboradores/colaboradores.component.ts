import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ColaboradoresDataSource } from './colaboradores-datasource';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'colaboradores',
  templateUrl: './colaboradores.component.html',
  styleUrls: ['./colaboradores.component.css']
})
export class ColaboradoresComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ColaboradoresDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name'];
  message: any;
  constructor(private userService:UserService){
    this.newMessage();
  }

  ngOnInit() {
    this.dataSource = new ColaboradoresDataSource(this.paginator, this.sort);
    this.userService.currentMessage.subscribe(message => this.message = message);
  }
  
  newMessage() {
    this.userService.changeMessage("Colaboradores");
  }
}
