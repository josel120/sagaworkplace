import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-bienvenidos',
  templateUrl: './bienvenidos.component.html',
  styleUrls: ['./bienvenidos.component.css']
})
export class BienvenidosComponent implements OnInit {
  message: any;

  constructor(private router:Router,private userService:UserService) { 
    this.newMessage();
  }

  ngOnInit() {
    this.redirectIfLogin();
    this.userService.currentMessage.subscribe(message => this.message = message)
  }

  redirectIfLogin(){
    if(localStorage.getItem('token') == null)
      this.router.navigate(["/login"]);    
  }
  newMessage() {
    this.userService.changeMessage("Bienvenidos");
  }

}
