import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { SoporteComponent } from '../../modal/soporte/soporte.component';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.css']
})
export class MyNavComponent {
  message:string;
  loggedIn: any;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    
  constructor(private breakpointObserver: BreakpointObserver, 
    public dialog: MatDialog,private router:Router, private _userService:UserService) {
      //this.loggedIn = this._userService.isLoggedIn;
      this.mostrar();
    }
    ngOnInit(): void {
      //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
      //Add 'implements OnInit' to the class.
      //this.mostrar();
      this._userService.currentMessage.subscribe(message => this.message = message)
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(SoporteComponent);
    }

    cerrarSesion(){
      //this._userService.cerrarSesion();
      localStorage.removeItem('token');
      this.router.navigate(["/login"]);
    }

    mostrar(){
      if(localStorage.getItem('token') != null){
        this.loggedIn = true;
      }else{
        this.loggedIn = false;
      }
    }
  
  
}
