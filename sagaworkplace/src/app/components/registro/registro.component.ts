import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MongoDBService } from '../../services/mongoDB.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  public user;
  registroForm: FormGroup;
  error: any;
  usuarios: any;
  public administradores;
  message: any;

  constructor(private userService:UserService,
              public toastr: ToastrService,
              private router:Router) {
                 
    this.userService.currentMessage.subscribe(message => this.message = message)
    this.newMessage();
               }

  ngOnInit() {
   // this.redirectIfLogin();
    this.validatorForm();
    this.listarAdministradores();
  }

  validatorForm(){
      this.registroForm = new FormGroup({ 
        'nombre': new FormControl(''),
        'email': new FormControl(''),
        'password': new FormControl(''),
        'password2': new FormControl('')
    });
  }

  crearAdmin(){
    if(this.registroForm.value.password == this.registroForm.value.password2){
      this.userService.registrarUser(this.registroForm.value).subscribe((response) => {
        this.toastr.success(response.message);
        this.listarAdministradores();
        //this.listarUsuarios();
        //this.router.navigate(['/bienvenidos']);
        //this.toastr.success(response['message'], 'Success');
      },
      error => {
        this.error = error; 
        this.toastr.error(this.error.error.message);
      });
    }else{
      console.log("Password No coinciden");
    }
  }
  
  listarAdministradores(){
    this.userService.listarAdmin().subscribe((x) => {
      //console.log(x);
      this.administradores = x ;
    })
  }

  eliminarAdministrador(data){
    this.userService.eliminarUser(data).subscribe((response) => {
      this.toastr.success(response.message);
      this.listarAdministradores();
      //console.log(response);
    },
    error => {
      this.error = error; 
      this.toastr.error(this.error.error.message);
    })
  }

  redirectIfLogin(){
    if(localStorage.getItem('token') == null)
      this.router.navigate(["/login"]);    
  }
  newMessage() {
    this.userService.changeMessage("Administradores");
  }
}
