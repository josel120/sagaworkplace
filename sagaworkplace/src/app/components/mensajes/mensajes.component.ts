import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styleUrls: ['./mensajes.component.css']
})
export class MensajesComponent implements OnInit {
  expression : boolean = false;
  emailForm:FormGroup;
  smsForm:FormGroup;
  message: any;

  constructor(private userService:UserService,private EmailformBuilder: FormBuilder,
    private SmsformBuilder: FormBuilder,private router:Router) {
      this.userService.currentMessage.subscribe(message => this.message = message)
      this.newMessage();
     }

  ngOnInit() {
   // this.redirectIfLogin();
    this.crearEmailForm();
    this.crearSmsForm();
  }
  
  crearEmailForm(){
    this.emailForm = this.EmailformBuilder.group({    
      correoArea: [{value:'', disabled: true}, Validators.required, ]
    });
  }
  crearSmsForm(){
    this.smsForm = this.SmsformBuilder.group({    
      smsArea: [{value:'', disabled: true}, Validators.required, ]
    });
  }
  isDisabledEmail(){    
      this.emailForm.get('correoArea').enable();
  }
  isDisabledSms(){    
    this.smsForm.get('smsArea').enable();
  }
  redirectIfLogin(){
    if(localStorage.getItem('token') == null)
      this.router.navigate(["/login"]);    
  }
  
  newMessage() {
    this.userService.changeMessage("Mensajes");
  }
}
