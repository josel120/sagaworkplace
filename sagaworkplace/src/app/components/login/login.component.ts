import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user;
  public identity;
  public codigo;
  public password;
  public error;
  

  constructor(public toastr: ToastrService,
    private router:Router,
    private userService:UserService) { 
      this.user = {
        "email":"",
        "password":""
      };
  }

  ngOnInit() {
    this.redirectIfLogin();
  }
  

  onSubmit(){
    this.userService.login(this.user).subscribe((response) => {
        this.toastr.success(response.message);      
        localStorage.setItem('token', response.token);
        this.router.navigate(["/bienvenidos"]);
      },
      error => {
        this.error = error; 
        this.toastr.error(this.error.error.message);
      }
    );
  }
  redirectIfLogin(){
    if(localStorage.getItem('token') != null)
      this.router.navigate(["/bienvenidos"]);    
  }

  olvidarPassword(email){
    console.log('olvidarPassword');
  }
  /*
  // abre el model colocar codigo
  openDialog(email): void {
    const dialogRef = this.dialog.open(OlvidarPasswordModelComponent, {
      data: {codigo: this.codigo}
    });    
    localStorage.setItem('email',email);
  }
  */
 /*
  cerrarSesion(){
    this.userService.cerrarSesion();
    localStorage.removeItem('token');
    this.router.navigate(["/login"]);
  }
*/
}
