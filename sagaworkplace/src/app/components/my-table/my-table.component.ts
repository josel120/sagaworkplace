import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { MyTableDataSource } from './my-table-datasource';
import { DatosArchivos } from '../../interfaces/datos-archivos';
import { SelectionModel } from '@angular/cdk/collections';
import { NgxSpinnerService } from 'ngx-spinner';
import { MongoDBService } from '../../services/mongoDB.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
//import { DataService } from '../../services/data.service';

@Component({
  selector: 'my-table',
  templateUrl: './my-table.component.html',
  styleUrls: ['./my-table.component.css']
})
export class MyTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MyTableDataSource;
  public usuarios; 
  private element_Data: DatosArchivos[];  
  message: string;

  constructor(private userService:UserService,private spinner: NgxSpinnerService, private mongoDBService:MongoDBService,
    private router:Router) { 
    this.element_Data = [];  
    this.userService.currentMessage.subscribe(message => this.message = message)
    this.newMessage();
  }
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select','username','nombre'];

  ngOnInit() {
    this.redirectIfLogin();
    this.spinner.show();
    this.usuarios = new MatTableDataSource(this.element_Data);
    this.usuarios.paginator = this.paginator;
    this.usuarios.sort = this.sort;
    this.listarUsuarios();
    
    //this.data.currentMessage.subscribe(message => this.message = message);
    
    //this.dataSource = new MyTableDataSource(this.paginator, this.sort);
  }
  selection = new SelectionModel<any>(true, []);
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.usuarios.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.usuarios.data.forEach(row => this.selection.select(row));
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.usuarios.filter = filterValue;
  }

  listarUsuarios(){
    this.mongoDBService.listUsersDB().subscribe((x) => {
      this.usuarios = x;
    });
  }
  
  redirectIfLogin(){
    if(localStorage.getItem('token') == null)
      this.router.navigate(["/login"]);    
  }
  
  newMessage() {
    this.userService.changeMessage("Administradores");
  }

}
