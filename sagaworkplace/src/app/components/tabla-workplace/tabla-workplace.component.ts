import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DatosWorplace } from '../../interfaces/datos-workplace';
import { NgxSpinnerService } from 'ngx-spinner';
import { SelectionModel } from '@angular/cdk/collections';
import { WorkplaceService } from '../../services/workplace.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-tabla-workplace',
  templateUrl: './tabla-workplace.component.html',
  styleUrls: ['./tabla-workplace.component.css']
})
export class TablaWorkplaceComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public usuarios; 
  private element_Data: DatosWorplace[];  
  message: string;
  error: any;

  constructor(private spinner: NgxSpinnerService, 
    private workplaceService:WorkplaceService,
    public toastr: ToastrService,private userService:UserService) { 
    this.element_Data = []; 
     
    this.userService.currentMessage.subscribe(message => this.message = message)
    this.newMessage();
  }
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select','username','nombre'];

  ngOnInit() {
    this.spinner.show();
    this.usuarios = new MatTableDataSource(this.element_Data);
    this.usuarios.paginator = this.paginator;
    this.usuarios.sort = this.sort;
    this.listarUsuarios();
    
    //this.data.currentMessage.subscribe(message => this.message = message);
    
    //this.dataSource = new MyTableDataSource(this.paginator, this.sort);
  }
  selection = new SelectionModel<any>(true, []);
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.usuarios.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.usuarios.data.forEach(row => this.selection.select(row));
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.usuarios.filter = filterValue;
  }

  /*
  listarUsuarios(){
    this.workplaceService.listarUsers().subscribe((response) => {
     // this.usuarios = response;
    },
    error => {
      this.error = error; 
      this.toastr.error(this.error.error.message);
    });
  }*/
  listarUsuarios(){
    this.workplaceService.listLUIS().subscribe((response) => {
      console.log(response.data.Resources);
      this.usuarios = response.data.Resources;
     },
     error => {
       this.error = error; 
       this.toastr.error(this.error.error.message);
     });
   }
   newMessage() {
    this.userService.changeMessage("Administradores");
  }
}
