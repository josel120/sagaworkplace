import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaWorkplaceComponent } from './tabla-workplace.component';

describe('TablaWorkplaceComponent', () => {
  let component: TablaWorkplaceComponent;
  let fixture: ComponentFixture<TablaWorkplaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaWorkplaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaWorkplaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
