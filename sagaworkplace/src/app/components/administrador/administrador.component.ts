import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {
  public administradores;
  error: any;
  message:string;

  constructor(private userService:UserService,
            public toastr: ToastrService,
            private router:Router) 
  {  
    this.userService.currentMessage.subscribe(message => this.message = message)
    this.newMessage();
  }

  ngOnInit() {
    this.redirectIfLogin();
    this.listarAdministradores();
  }

  listarAdministradores(){
    this.userService.listarAdmin().subscribe((x) => {
      //console.log(x);
      this.administradores = x ;
    })
  }

  eliminarAdministrador(data){
    this.userService.eliminarUser(data).subscribe((response) => {
      this.toastr.success(response.message);
      this.listarAdministradores();
      //console.log(response);
    },
    error => {
      this.error = error; 
      this.toastr.error(this.error.error.message);
    })
  }
  redirectIfLogin(){
    if(localStorage.getItem('token') == null)
      this.router.navigate(["/login"]);    
  }
  newMessage() {
    this.userService.changeMessage("Administradores");
  }

}
